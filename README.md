# school-notes

<!-- **\[AVISO\] Recomendo ver o projecto no [gitlab](https://gitlab.com/OnikenX/school-notes)** caso esteja a ler isto no github por causa da syntax matematica não é processada.
-->
Este repo serve para guardar os meus apontamentos e recursos para estudar.

## Anotações

[Notas de Eletro](./Eletro/notas.md)

[Notas de Metodos Estatisticos](./Metodos-Estatisticos/notas.md)
